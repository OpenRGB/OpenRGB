const path = require("path");
const process = require("process");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const devtool = process.NODE_ENV === "development" ? "eval-source-map" : "source-map"

module.exports = {
    entry: "./app/index.js",
    devtool: devtool,
    target: "electron-renderer",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/env", "@babel/react"],
                        plugins: ["@babel/plugin-proposal-class-properties"]
                    }
                }
            }
        ]
    },

    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "app", "dist")
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: "./app/index.html",
            filename: "./index.html",
        }),
        new webpack.HotModuleReplacementPlugin()
    ],

    devServer: {
        contentBase: path.join(__dirname, "app"),
        compress: true,
        historyApiFallback: true,
        hot: true
    }
}