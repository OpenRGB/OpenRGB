const {app, BrowserWindow, ipcMain} = require("electron");
const WebSocket = require("ws");
const settings = require("electron-settings");
const Splashscreen = require("@trodi/electron-splashscreen");
const process = require("process");
const path = require("path");
const request = require("request");
const OpenRGB = require("node-openrgb");
const http = require("http");

// Global device variables
let g_hue;
process.on('unhandledRejection', console.error);

let win;

app.on("ready", () => {
    global.devices = [];
    win = Splashscreen.initSplashScreen({
        windowOpts: {width: 720, height: 720, title: "OpenRGB"},
        templateUrl: path.join(__dirname, "icon.svg"),
        delay: 0,
        splashScreenOpts: {
            height: 500,
            width: 500,
            transparent: true
        }
    });

    win.webContents.toggleDevTools();

    let loadWindow = function() {
        if (process.env.NODE_ENV === "development") {
            // Wait until Webpack finishes
            const url = "http://127.0.0.1:8080/index.html"
            const interval = setInterval(() => {
                request.get(url, (err) => {
                    if (!err) {
                        clearInterval(interval);
                        win.loadURL(url);
                    }
                })
            }, 1000)
        } else {
            win.loadFile("app/dist/index.html");
        }
    }

    detectDevices().then((devices) => {
        global.devices = devices;
        loadWindow();
    }).catch((err) => {
        loadWindow();
    });

    ipcMain.on("settingsSet", (event, args) => {
        console.log(args);
        settings.set(args.name, args.data);
    })
    
    ipcMain.on("settingsGet", (event, args) => {
        console.log(settings.get(args))
        event.sender.send("settingsGet", settings.get(args));
    })
    settings.set("wsEnabled", true);

    if (settings.get("wsEnabled") === true) {
        server();
    }
    jsonServer();
    
    win.on("closed", () => {
        // settings.deleteAll();
        win = null;
    });
});

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("active", () => {
    if (win === null) {
        createWindow();
    }
});

ipcMain.on("detectDevices", (event, args) => {
    detectDevices().then(event.sender.send);
});

function server() {
    const wss = new WebSocket.Server({port: 6854});
    wss.on("connection", (ws) => {
        ws.on("message", (msg) => {
            msg = JSON.parse(msg);
            console.log(msg);
            if (msg.device === "hueplus") {
                if (msg.command === "changeColor") {
                    const data = msg.data;
                    g_hue.changeColor(this.state.ch1.color, data.mode, data.channel, data.speed, data.size, data.moving, data.backwards, data.custom).catch((err) => {
                        ws.send(JSON.stringify({result: "error", error: err}));
                    }).then(() => {
                        ws.send(JSON.stringify({result: "success"}));
                        win.webContents.send("hueplus_updateState", msg.data);
                    })
                } else if (msg.command === "turnOffLED") {
                    g_hue.turnOffLED().then(() => {ws.send(JSON.stringify({result: "success"}))}).catch((err) => {
                        ws.send(JSON.stringify({result: "error", error: err}));
                    });
                } else if (msg.command === "turnOnLED") {
                    g_hue.turnOnLED().then(() => {ws.send(JSON.stringify({result: "success"}))}).catch((err) => {
                        ws.send(JSON.stringify({result: "error", error: err}));
                    });
                }
                
            }
        })
    })
}

function jsonServer() {
    const port = 6855;
    const requestHandler = (req, res) => {
        console.log(req);
        res.end("ok");
    }
    const jServer = http.createServer(requestHandler);
    jServer.listen(port, (err) => {
        if (err) { return console.error("Uh-oh, server made a fucky wucky", err) };
        console.log("Server is listening on", port);
    })
}

function detectDevices() {
    return new Promise(async (resolve, reject) => {
        let devices = {};

        // NZXT HUE+
        try {
            const hueplus = await new OpenRGB.HuePlus();
            devices["hueplus"] = true;
            global.hueplus_ledCount = { ch1: hueplus.ledCount.ch1.count, ch2: hueplus.ledCount.ch2.count };
            global.hueplus_type = { ch1: hueplus.ledCount.ch1.type, ch2: hueplus.ledCount.ch2.type };
            hueplus_events(hueplus);
            g_hue = hueplus;
        } catch (err) {
            console.log(err);
            if (err.name === "DeviceNotFound") return;
            devices["hueplus"] = err;
        }

        // NZXT Smart Device
        try {
            const SmartDevice = await new OpenRGB.NZXT_SmartDevice();
            devices["nzxt_smartdevice"] = true;
            smartdevice_events(SmartDevice);
        } catch (err) {
            if (err.name === "DeviceNotFound") return;
            devices["nzxt_smartdeivce"] = err;
        }

        resolve(devices);
    });
}

function hueplus_events(hue) {
    ipcMain.on("hueplus_changeColor", (event, args) => {
        hue.changeColor(args.color, args.mode, args.channel, args.speed, args.size, args.moving, args.backwards, args.custom)
            .then(() => event.sender.send("hueplus_changeColor", true))
            .catch((err) => {
                console.error(err);
                event.sender.send("hueplus_changeColor", err.message);
            });
    });

    ipcMain.on("hueplus_turnOffLED", (event, args) => {
        hue.turnOffLED().then(() => event.sender.send("hueplus_turnOffLED", true)).catch((err) => event.sender.send("hueplus_turnOffLED", err.message));
    });

    ipcMain.on("hueplus_turnOnLED", (event, args) => {
        hue.turnOnLED().then(() => event.sender.send("hueplus_turnOnLED", true)).catch((err) => event.sender.send("hueplus_turnOnLED", err.message));
    });
}

function smartdevice_events(device) {
    device.on("rpmChange", (data) => {
        win.webContents.send("rpmChange", data);
    });

    ipcMain.on("smartdevice_changeColor", (event, args) => {
        try {
            device.changeColor(args.color, args.mode, args.speed, args.size, args.moving, args.backwards, args.custom);
            event.sender.send("smartdevice_changeColor", true);
        } catch (err) {
            event.sender.send("smartdevice_changeColor", err.message);
        }
    });

    ipcMain.on("smartdevice_setFanSpeed", (event, args) => {
        try {
            device.setFanSpeed(args.fan, args.percent);
            event.sender.send("smartdevice_setFanSpeed", true)
        } catch(err) {
            event.sender.send("smartdevice_setFanSpeed", err.message)
        }
    });
}