const WebSocket = require("ws");
const ws = new WebSocket("ws://127.0.0.1:6854");
ws.on("open", () => {
    ws.send(JSON.stringify({
        device: "hueplus",
        command: "changeColor",
        data: {
            color: "ffffff",
            mode: "fixed",
            channel: 2,
            // speed: 3
        }
    }));
    ws.on("message", (msg) => {
        console.log(msg);
        ws.close();
    });
})
