import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import SwipeableViews from "react-swipeable-views";
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { ChevronLeft, ChevronRight, Home, WbSunnyOutlined, Menu, Settings } from "@material-ui/icons";
import { AppBar, Divider, Drawer, IconButton, List, ListItem, ListItemIcon, ListItemText, Toolbar, Typography, Paper} from '@material-ui/core';
import SettingsDialog from "./SettingsDialog";

const drawerWidth = 240;

const lightTheme = createMuiTheme({
  palette: {
    type: "light",
    primary: {
        light: "#42A5F5",
        main: "#1E88E5",
        dark: "#1565C0",
        contrastText: "#fff"
    },
    secondary: {
      light: "#FFA726",
      main: "#FF9800",
      dark: "#EF6C00",
      contrastText: "#fff"
    }
  },
  typography: {
    useNextVariants: true
  }
});

const darkTheme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
        light: "#4FC3F7",
        main: "#03A9F4",
        dark: "0288D1",
        contrastText: "#fff"
    }
  },
})

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginBottom: 0
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    // padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default
  },
  flex: {
    flex: 1
  }
});

class Main extends React.Component {
  state = {
    open: false,
    settingsDialogOpen: false,
    index: 0
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleSettingsDialogOpen = () => {
    this.setState({settingsDialogOpen: true});
  }

  handleSettingsDialogClose = () => {
    this.setState({settingsDialogOpen: false})
  }

  handleChangeIndex = (idx) => {
    this.setState({index: idx});
  }

  render() {
    const { classes, theme } = this.props;

    return (
      <MuiThemeProvider theme={lightTheme}>
      <div className={classes.root}>
        
        <AppBar
          position="absolute"
          className={classNames(classes.appBar, this.state.open && classes.appBarShift)}
          color="primary"
          style={{margin: 0}}
        >
          <SettingsDialog open={this.state.settingsDialogOpen} onClose={this.handleSettingsDialogClose} />
          <Toolbar disableGutters={!this.state.open}>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={this.handleDrawerOpen}
              className={classNames(classes.menuButton, this.state.open && classes.hide)}
            >
              <Menu />
            </IconButton>
            <Typography variant="h6" color="inherit" className={classes.flex} noWrap>
              OpenRGB
            </Typography>
            <IconButton onClick={this.handleSettingsDialogOpen}>
              <Settings color="inherit" />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
          }}
          open={this.state.open}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={this.handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRight /> : <ChevronLeft />}
            </IconButton>
          </div>
          <Divider />
          <List>
            <ListItem button onClick={() => this.handleChangeIndex(0)}>
              <ListItemIcon><Home /></ListItemIcon>
              <ListItemText>Home</ListItemText>
            </ListItem>
            <ListItem button onClick={() => { this.handleChangeIndex(1); } }>
              <ListItemIcon><WbSunnyOutlined /></ListItemIcon>
              <ListItemText>Lighting</ListItemText>
            </ListItem>
          </List>
        </Drawer>
        <main className={classes.content} ref="main">
          <div className={classes.toolbar} />
            <Paper elevation={1} style={{height: "calc(100% - 64px)"}} square={true}>
              {/* TODO: make the swipeable views vertical */}
              <SwipeableViews index={this.state.index} onChangeIndex={this.handleChangeIndex}>
                {React.Children.map(this.props.children, (element, idx) => {
                  return React.cloneElement(element, { ref: idx });
                })}
              </SwipeableViews>
            </Paper>
        </main>
      </div>
      </MuiThemeProvider>
    );
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true})(Main);