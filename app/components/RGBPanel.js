import React from "react";
import HuePlus from "./RGB/HuePlus";
import NZXT_SmartDevice from "./RGB/NZXT_SmartDevice";
import ErrorDevice from "./RGB/ErrorDevice";
import { Paper, Typography } from "@material-ui/core";
import { remote } from "electron"

export default class RGBPanel extends React.Component {
    state = {
        devices: remote.getGlobal("devices")
    }

    checkForErrors = () => {
        for (let i = 0; i < Object.keys(this.state.devices).length; i++) {
            if (typeof Object.values(this.state.devices)[i] !== "boolean") {
                return <ErrorDevice error={Object.values(this.state.devices)[i]} />
            }
        }
    }

    render() {
        return (
            <div style={{marginTop: 10}}>
                {Object.keys(this.state.devices).length === 0 && <Typography variant="title" align="center">No devices were detected.</Typography>}
                {this.state.devices.hueplus === true && <HuePlus />}
                {this.state.devices.nzxt_smartdevice === true && <NZXT_SmartDevice />}
                {this.checkForErrors()}
            </div>
        )
    }
}