import React from "react"
import { Button, ExpansionPanel, ExpansionPanelActions, ExpansionPanelDetails, ExpansionPanelSummary, Typography, withStyles, FormControl, MenuItem, InputLabel, Select, Switch, Divider, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, FormControlLabel, Checkbox  } from "@material-ui/core";
import { ExpandMore } from "@material-ui/icons";
import { ipcRenderer, remote } from "electron";
import { ChromePicker } from "react-color";

const styles = theme => ({
    root: {
        display: "flex"
    },
    heading: {
        fontSize: theme.typography.pxToRem(18),
        flexBasis: "33.33%",
        flexShrink: 0
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(18),
        color: theme.palette.text.secondary
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
})

class HuePlus extends React.Component {
    state = {
        unitLedEnabled: true,
        ch1: {
            mode: "fixed",
            color: "ffffff",
            channel: 1,
            speed: 3,
            size: 3,
            backwards: false,
            moving: false,
            custom: false,
        },
        ch2: {
            mode: "fixed",
            color: "ffffff",
            channel: 2,
            speed: 3,
            size: 3,
            backwards: false,
            moving: false,
            custom: false,
        },
        applyButtonDisabled: false,
        errorMessage: "",
        errorDialogOpen: false
    }
    
    // All apply functions have a setTimeout because of a race condition
    init = () => {
        ipcRenderer.once("settingsGet", (event, args) => {
            if (args !== null) {
                console.log("Previous settings: ", args);
                setTimeout(() => this.apply(1).then(() => this.apply(2)).then(() => { if (!args.unitLedEnabled) ipcRenderer.send("hueplus_turnOffLED"); }).catch(this.errorDialogOpen), 10);
                this.setState(args);
            }
        })
        ipcRenderer.send("settingsGet", "previousColor");
    }

    handleChange1 = (event) => {
        this.setState(prevState => ({
            ch1: {
                ...prevState.ch1,
                [event.target.name]: event.target.value
            }
        }))
        setTimeout(() => this.apply(1).then(() => this.apply(2)).catch(this.errorDialogOpen), 10);
    }

    handleChange2 = (event) => {
        this.setState(prevState => ({
            ch2: {
                ...prevState.ch2,
                [event.target.name]: event.target.value
            }
        }))
        setTimeout(() => this.apply(1).then(() => this.apply(2)).catch(this.errorDialogOpen), 10);
    }

    errorDialogOpen = (error) => {
        this.setState({errorMessage: error, errorDialogOpen: true});
    }

    errorDialogClose = () => {
        this.setState({errorDialogOpen: false});
    }

    handleColorChange1 = (_color, event) => {
        this.setState(prevState => ({
            ch1: {
                ...prevState.ch1,
                color: _color.hex
            }
        }))
        setTimeout(() => this.apply(1).then(() => this.apply(2)).catch(this.errorDialogOpen), 10);
    }

    handleColorChange2 = (_color, event) => {
        this.setState(prevState => ({
            ch2: {
                ...prevState.ch2,
                color: _color.hex
            }
        }))
        setTimeout(() => this.apply(1).then(() => this.apply(2)).catch(this.errorDialogOpen), 10);
    }

    saveSettings = () => {
        let data = this.state;
        delete data.errorMessage;
        delete data.errorDialogOpen;
        delete data.applyButtonDisabled;
        ipcRenderer.send("settingsSet", {name: "previousColor", data: data});
    }

    apply = (channel) => {
        return new Promise((resolve, reject) => {
            ipcRenderer.once("hueplus_changeColor", (event, args) => {
                if (args !== true) {
                    reject(args)
                } else {
                    this.saveSettings();
                    this.setState({applyButtonDisabled: false});
                    resolve(true)
                }
            })
            ipcRenderer.send("hueplus_changeColor", this.state["ch" + channel]);
            this.setState({applyButtonDisabled: true});
        })
        
    }

    handleUnitLED = (event, checked) => {
        this.setState({unitLedEnabled: checked});
        if (!this.state.unitLedEnabled) {
            ipcRenderer.once("hueplus_turnOnLED", (event, args) => {
                if (args !== true) this.errorDialogOpen(args);
                this.saveSettings();
            })
            ipcRenderer.send("hueplus_turnOnLED");
        } else {
            ipcRenderer.once("hueplus_turnOffLED", (event, args) => {
                if (args !== true) this.errorDialogOpen(args);
                this.saveSettings();
            })
            ipcRenderer.send("hueplus_turnOffLED");
        }
    }

    handleCheckbox1 = name => event => {
        let checked = event.target.checked // Must be defined as a variable or it will return null
        this.setState(prevState => ({
            ch1: {
                ...prevState.ch1,
                [name]: checked
            }
        }))
        setTimeout(() => this.apply(1).then(() => this.apply(2)).catch(this.errorDialogOpen), 10);
    }

    handleCheckbox2 = name => event => {
        let checked = event.target.checked // Must be defined as a variable or it will return null
        this.setState(prevState => ({
            ch2: {
                ...prevState.ch2,
                [name]: checked
            }
        }))
        setTimeout(() => this.apply(1).then(() => this.apply(2)).catch(this.errorDialogOpen), 10);
    }

    componentDidMount = () => {
        this.init();
        ipcRenderer.on("hueplus_updateState", (event, args) => {
            this.setState(args);
        })
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                <Dialog open={this.state.errorDialogOpen} onClose={this.errorDialogClose}>
                    <DialogTitle>An error occured</DialogTitle>
                    <DialogContent>
                        <DialogContentText>{this.state.errorMessage}</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.errorDialogClose} color="primary">Ok</Button>
                    </DialogActions>
                </Dialog>
                { remote.getGlobal("hueplus_ledCount").ch1 > 0 ?
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                        <Typography className={classes.heading}>NZXT HUE+</Typography>
                        <Typography className={classes.secondaryHeading}>{`Channel 1: ${remote.getGlobal("hueplus_ledCount").ch1} ${(remote.getGlobal("hueplus_type").ch1 === "led" ? "LED" : "Fan") + (remote.getGlobal("hueplus_ledCount").ch1 > 1 ? "s" : "")} `}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <FormControlLabel control={<Switch checked={this.state.unitLedEnabled} onChange={this.handleUnitLED} /> } label="Toggle Unit LED" />
                    </ExpansionPanelDetails>
                    <ExpansionPanelDetails>
                        <FormControl className={classes.formControl}>
                            <InputLabel>Mode</InputLabel>
                            <Select value={this.state.ch1.mode} onChange={this.handleChange1} inputProps={{name: "mode"}}>
                                <MenuItem value={"fixed"}>Fixed</MenuItem>
                                <MenuItem value={"breathing"}>Breathing</MenuItem>
                                <MenuItem value={"fading"}>Fading</MenuItem>
                                <MenuItem value={"spectrum"}>Spectrum</MenuItem>
                                <MenuItem value={"marquee"}>Marquee</MenuItem>
                                <MenuItem value={"coveringMarquee"}>Covering Marquee</MenuItem>
                                <MenuItem value={"alternating"}>Alternating</MenuItem>
                                <MenuItem value={"candle"}>Candle</MenuItem>
                                <MenuItem value={"wings"}>Wings</MenuItem>
                            </Select>
                        </FormControl>
                        {this.state.ch1.mode !== "fixed" && this.state.ch1.mode !== "candle" && <FormControl className={classes.formControl}>
                            <InputLabel>Speed</InputLabel>
                            <Select value={this.state.ch1.speed} onChange={this.handleChange1} inputProps={{name: "speed"}}>
                                <MenuItem value={1}>Slowest</MenuItem>
                                <MenuItem value={2}>Slower</MenuItem>
                                <MenuItem value={3}>Normal</MenuItem>
                                <MenuItem value={4}>Faster</MenuItem>
                                <MenuItem value={5}>Fastest</MenuItem>
                            </Select>
                        </FormControl> }
                        {(this.state.ch1.mode === "marquee" || this.state.ch1.mode === "coveringMarquee" || this.state.ch1.mode === "spectrum" || this.state.ch1.mode === "alternating") && 
                            <FormControlLabel control={<Checkbox value="backwards" onChange={this.handleCheckbox1("backwards")} />} label="Backwards"/> }
                        {this.state.ch1.mode === "alternating" && <FormControlLabel control={<Checkbox value="moving" onChange={this.handleCheckbox1("moving")} />} label="Moving"/> }
                    </ExpansionPanelDetails>
                    <ExpansionPanelDetails>
                        {this.state.ch1.mode !== "spectrum" ? <ChromePicker color={this.state.ch1.color} onChangeComplete={this.handleColorChange1} disableAlpha={true} className="yeet" /> : ""}
                    </ExpansionPanelDetails>
                    <Divider />
                </ExpansionPanel> : ""}
                {remote.getGlobal("hueplus_ledCount").ch2 > 0 ?
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                        <Typography className={classes.heading}>NZXT HUE+</Typography>
                        <Typography className={classes.secondaryHeading}>{`Channel 2: ${remote.getGlobal("hueplus_ledCount").ch2} ${(remote.getGlobal("hueplus_type").ch2 === "led" ? "LED" : "Fan") + (remote.getGlobal("hueplus_ledCount").ch2 > 1 ? "s" : "")} `}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <FormControlLabel control={<Switch checked={this.state.unitLedEnabled} onChange={this.handleUnitLED} /> } label="Toggle Unit LED" />
                    </ExpansionPanelDetails>
                    <ExpansionPanelDetails>
                        <FormControl className={classes.formControl}>
                            <InputLabel>Mode</InputLabel>
                            <Select value={this.state.ch2.mode} onChange={this.handleChange2} inputProps={{name: "mode"}}>
                                <MenuItem value={"fixed"}>Fixed</MenuItem>
                                <MenuItem value={"breathing"}>Breathing</MenuItem>
                                <MenuItem value={"fading"}>Fading</MenuItem>
                                <MenuItem value={"spectrum"}>Spectrum</MenuItem>
                                <MenuItem value={"marquee"}>Marquee</MenuItem>
                                <MenuItem value={"coveringMarquee"}>Covering Marquee</MenuItem>
                                <MenuItem value={"alternating"}>Alternating</MenuItem>
                                <MenuItem value={"candle"}>Candle</MenuItem>
                                <MenuItem value={"wings"}>Wings</MenuItem>
                            </Select>
                        </FormControl>
                        {this.state.ch2.mode !== "fixed" && this.state.ch2.mode !== "candle" && <FormControl className={classes.formControl}>
                            <InputLabel>Speed</InputLabel>
                            <Select value={this.state.ch2.speed} onChange={this.handleChange2} inputProps={{name: "speed"}}>
                                <MenuItem value={1}>Slowest</MenuItem>
                                <MenuItem value={2}>Slower</MenuItem>
                                <MenuItem value={3}>Normal</MenuItem>
                                <MenuItem value={4}>Faster</MenuItem>
                                <MenuItem value={5}>Fastest</MenuItem>
                            </Select>
                        </FormControl> }
                        {(this.state.ch2.mode === "marquee" || this.state.ch2.mode === "coveringMarquee" || this.state.ch2.mode === "spectrum" || this.state.ch2.mode === "alternating") && 
                            <FormControlLabel control={<Checkbox value="backwards" onChange={this.handleCheckbox2("backwards")} />} label="Backwards"/> }
                        {this.state.ch2.mode === "alternating" && <FormControlLabel control={<Checkbox value="moving" onChange={this.handleCheckbox2("moving")} />} label="Moving"/> }
                    </ExpansionPanelDetails>
                    <ExpansionPanelDetails>
                    {this.state.ch2.mode !== "spectrum" ? <ChromePicker color={this.state.ch2.color} onChangeComplete={this.handleColorChange2} disableAlpha={true} /> : ""}
                    </ExpansionPanelDetails>
                    <Divider />
                </ExpansionPanel> : ""}
            </div>
        )
    }
}

export default withStyles(styles, {withTheme: true})(HuePlus);