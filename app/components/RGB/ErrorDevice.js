import React from "react";
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Typography, withStyles, Paper } from "@material-ui/core";
import { Error } from "@material-ui/icons";

const styles = theme => ({
    heading: {
        fontSize: theme.typography.pxToRem(18),
        fontWeight: theme.typography.fontWeightRegular,
        flexBasis: "33.33%",
        flexShrink: 0
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(18),
        fontWeight: theme.typography.fontWeighThin
    }
})

class ErrorDevice extends React.Component {
    render() {
        const { classes } = this.props;
        console.log(this.props)
        return(
            <ExpansionPanel>
                <ExpansionPanelSummary expandIcon={<Error color="error" />}>
                    <Typography className={classes.heading}>{this.props.error.deviceName}</Typography>
                    <Typography className={classes.secondaryHeading}>An error occured</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography style={{width: "100%"}}>
                        {`An error occured while accessing the ${this.props.error.deviceName}: \"${this.props.error.message}\"`} <br />
                        {["hueplus", "nzxt_smartdevice"].includes(this.props.error.deviceID) && "If you received an access/permission denied error, it can usually be fixed by exiting CAM on Windows, or correcting your permissions to the device on macOS/Linux (refer to the docs)."}
                    </Typography>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        )
    }
}

export default withStyles(styles, {withTheme: true})(ErrorDevice);