import React from "react"
import { Button, ExpansionPanel, ExpansionPanelActions, ExpansionPanelDetails, ExpansionPanelSummary, Typography, withStyles, FormControl, MenuItem, InputLabel, Select, Switch, Divider, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, FormControlLabel, Checkbox, GridList, GridListTile, Grid, Toolbar, Tooltip  } from "@material-ui/core";
import { ExpandMore } from "@material-ui/icons";
import { ipcRenderer, remote } from "electron";
import { ChromePicker } from "react-color";
import Slider from "@material-ui/lab/Slider";

const styles = theme => ({
    root: {
        display: "flex"
    },
    heading: {
        fontSize: theme.typography.pxToRem(18),
        flexBasis: "33.33%",
        flexShrink: 0
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(18),
        color: theme.palette.text.secondary
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
})

class HuePlus extends React.Component {
    state = {
        // RGB
        mode: "fixed",
        color: "ffffff",
        channel: 1,
        speed: 3,
        size: 3,
        backwards: false,
        moving: false,
        custom: false,
        
        // Fans
        fan1RPM: 0,
        fan2RPM: 0,
        fan3RPM: 0,
        fan1: 50,
        fan2: 50,
        fan3: 50,
        allFans: false,

        applyButtonDisabled: false,
        errorMessage: "",
        errorDialogOpen: false,
    }
    
    // All apply functions have a setTimeout because of a race condition
    init = () => {
        ipcRenderer.on("rpmChange", (event, args) => {
            let fan = "fan" + args[0] + "RPM";
            this.setState({[fan]: args[1]});
        })
        ipcRenderer.once("settingsGet", (event, args) => {
            console.log(args)
            if (args !== null) {
                console.log("Previous settings: ", args);
                this.setState(args);
            }
            setTimeout(() => this.apply().catch(this.errorDialogOpen), 10);
            setTimeout(() => this.applyFan(), 10);
        })
        ipcRenderer.send("settingsGet", "smartdevice_previousSettings");
    }

    handleChange = (event, value) => {
        this.setState({[event.target.name]: event.target.value});
        setTimeout(() => this.apply().catch(this.errorDialogOpen), 10);
    }

    // I really wish I didn't have to do this, but the Slider API does not have a name prop :(
    handleSlider1 = (event, value) => {
        this.setState({fan1: value});
        if (this.state.allFans) this.setState({fan2: value, fan3: value});
    }

    handleSlider2 = (event, value) => {
        this.setState({fan2: value});
    }

    handleSlider3 = (event, value) => {
        this.setState({fan3: value});
    }

    errorDialogOpen = (error) => {
        this.setState({errorMessage: error, errorDialogOpen: true});
    }

    errorDialogClose = () => {
        this.setState({errorDialogOpen: false});
    }

    handleColorChange = (_color, event) => {
        this.setState({color: _color.hex});
        setTimeout(() => this.apply().catch(this.errorDialogOpen), 10);
    }

    saveSettings = () => {
        let data = this.state;
        delete data.errorMessage;
        delete data.errorDialogOpen;
        delete data.applyButtonDisabled;
        delete data.fan1RPM;
        delete data.fan2RPM;
        delete data.fan3RPM;
        ipcRenderer.send("settingsSet", {name: "smartdevice_previousSettings", data: data});
    }

    apply = () => {
        return new Promise((resolve, reject) => {
            ipcRenderer.once("smartdevice_changeColor", (event, args) => {
                if (args !== true) {
                    reject(args)
                } else {
                    this.saveSettings();
                    this.setState({applyButtonDisabled: false});
                    resolve(true)
                }
            })
            ipcRenderer.send("smartdevice_changeColor", this.state);
            this.setState({applyButtonDisabled: true});
        })
    }

    applyFan = () => {
        for (let i = 0; i < 3; i++) {
            ipcRenderer.once("smartdevice_setFanSpeed", (event, args) => {
                if (args !== true) return this.errorDialogOpen(args);
                else this.saveSettings();
            });
            ipcRenderer.send("smartdevice_setFanSpeed", {fan: i + 1, percent: this.state["fan" + (i + 1)]});
        }
    }

    handleCheckbox = name => event => {
        let checked = event.target.checked // Must be defined as a variable or it will return null
        this.setState({[name]: checked});
        setTimeout(() => this.apply().catch(this.errorDialogOpen), 10);
    }

    componentDidMount = () => {
        this.init();
        ipcRenderer.on("hueplus_updateState", (event, args) => {
            this.setState(args);
        })
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                <Dialog open={this.state.errorDialogOpen} onClose={this.errorDialogClose}>
                    <DialogTitle>An error occured</DialogTitle>
                    <DialogContent>
                        <DialogContentText>{this.state.errorMessage}</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.errorDialogClose} color="primary">Ok</Button>
                    </DialogActions>
                </Dialog>
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                        <Typography className={classes.heading}>NZXT Smart Device</Typography>
                        <Typography className={classes.secondaryHeading}>RGB Control</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <FormControl className={classes.formControl}>
                            <InputLabel>Mode</InputLabel>
                            <Select value={this.state.mode} onChange={this.handleChange} inputProps={{name: "mode"}}>
                                <MenuItem value={"fixed"}>Fixed</MenuItem>
                                <MenuItem value={"breathing"}>Breathing</MenuItem>
                                <MenuItem value={"fading"}>Fading</MenuItem>
                                <MenuItem value={"spectrum"}>Spectrum</MenuItem>
                                <MenuItem value={"marquee"}>Marquee</MenuItem>
                                <MenuItem value={"coveringMarquee"}>Covering Marquee</MenuItem>
                                <MenuItem value={"alternating"}>Alternating</MenuItem>
                                <MenuItem value={"candle"}>Candle</MenuItem>
                                <MenuItem value={"wings"}>Wings</MenuItem>
                            </Select>
                        </FormControl>
                        {this.state.mode !== "fixed" && this.state.mode !== "candle" && <FormControl className={classes.formControl}>
                            <InputLabel>Speed</InputLabel>
                            <Select value={this.state.speed} onChange={this.handleChange} inputProps={{name: "speed"}}>
                                <MenuItem value={1}>Slowest</MenuItem>
                                <MenuItem value={2}>Slower</MenuItem>
                                <MenuItem value={3}>Normal</MenuItem>
                                <MenuItem value={4}>Faster</MenuItem>
                                <MenuItem value={5}>Fastest</MenuItem>
                            </Select>
                        </FormControl> }
                        {(this.state.mode === "marquee" || this.state.mode === "coveringMarquee" || this.state.mode === "spectrum" || this.state.mode === "alternating") && 
                            <FormControlLabel control={<Checkbox value="backwards" onChange={this.handleCheckbox("backwards")} />} label="Backwards"/> }
                        {this.state.mode === "alternating" && <FormControlLabel control={<Checkbox value="moving" onChange={this.handleCheckbox("moving")} />} label="Moving"/> }
                    </ExpansionPanelDetails>
                    <ExpansionPanelDetails>
                        {this.state.mode !== "spectrum" ? <ChromePicker color={this.state.color} onChangeComplete={this.handleColorChange} disableAlpha={true} className="yeet" /> : ""}
                    </ExpansionPanelDetails>
                    <Divider />
                </ExpansionPanel>
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                        <Typography className={classes.heading}>NZXT Smart Device</Typography>
                        <Typography className={classes.secondaryHeading}>Fan Control</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Grid container spacing={24}>
                            <Grid item xs={12} md={12}>
                                <FormControlLabel control={<Checkbox value="allFans" onChange={this.handleCheckbox("allFans")} />} label="Select all fans"/>
                            </Grid>
                            <Grid item xs={8} md={8}>
                                <Typography variant="h5" style={{marginBottom: 10}}>Fan 1: {this.state.fan1RPM} RPM</Typography>
                                <Tooltip title={this.state.fan1 + "%"}>
                                    <Slider value={this.state.fan1} step={1} min={0} max={100} name="fan1" onChange={this.handleSlider1} onDragEnd={this.applyFan} />
                                </Tooltip>
                            </Grid>
                            <Grid item xs={8} md={8}>
                                <Typography variant="h5" style={{marginBottom: 10}}>Fan 2: {this.state.fan2RPM} RPM</Typography>
                                <Tooltip title={this.state.fan2 + "%"}>
                                    <Slider value={this.state.fan2} step={1} min={0} max={100} onChange={this.handleSlider2} disabled={this.state.allFans} onDragEnd={this.applyFan}/>
                                </Tooltip>
                            </Grid>
                            <Grid item xs={8} md={8}>
                                <Typography variant="h5" style={{marginBottom: 10}}>Fan 3: {this.state.fan3RPM} RPM</Typography>
                                <Tooltip title={this.state.fan3 + "%"}>
                                    <Slider value={this.state.fan3} step={1} min={0} max={100} onChange={this.handleSlider3} disabled={this.state.allFans} onDragEnd={this.applyFan}/>
                                </Tooltip>
                            </Grid>
                        </Grid>
                    </ExpansionPanelDetails>
                    <Divider />
                </ExpansionPanel>
            </div>
        )
    }
}

export default withStyles(styles, {withTheme: true})(HuePlus);