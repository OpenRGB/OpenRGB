import React from "react";
import ReactDOM from "react-dom";
import Main from "./components/Main";
import SwipeableViews from "react-swipeable-views";
import RGBPanel from "./components/RGBPanel";

export default class App extends React.Component {
    render() {
        return (
            <Main>
                <div style={{position: "relative"}}>
                    <h1 style={{width: "100%"}}>Hello world!</h1>
                </div>
                <div style={{position: "relative"}}>
                    <RGBPanel />
                </div>
            </Main>
        )
    }
}

ReactDOM.render(<App />, document.getElementById("root"));